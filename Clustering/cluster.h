#ifndef CLUSTER_H_
#define CLUSTER_H_

#define PI 3.14

#include "stdlib.h"
#include "math.h"
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;


//data structures
class Feature{
public:
	double posx;
	double posy;
	double grad;
	double orient;
	int uni_num;
	Feature(){};
	Feature(double,double,double,double,int);
	void print();

};

class FeaturePair{
public:
	Feature f1;
	Feature f2;
	double dist;
	FeaturePair(){};
	FeaturePair(Feature,Feature,double);
};

//feature operations
//double findFeatureDist(Feature f1,Feature f2);

//cluster
//Feature cluster(Feature f1,Feature f2);


list<Feature> doClustering(list<Feature> &points,int unique_num,int c);

#endif /* CLUSTER_H_ */
