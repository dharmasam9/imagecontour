#include "cluster.h"

string intToString(int x){
	 ostringstream convert;
	  convert << x;
	  string res = convert.str();
	  return res;
}

void sobel_us(Mat &input,Mat &grad,int x){

	// Construct kernel (all entries initialized to 0)
	cv::Mat kernel(3,3,CV_32F,cv::Scalar(0));
	if(x == 0){
	// assigns kernel values
	kernel.at<float>(0,0)= -1.0;
	kernel.at<float>(1,0)= -2.0;
	kernel.at<float>(2,0)= -1.0;
	kernel.at<float>(0,2)= 1.0;
	kernel.at<float>(1,2)= 2.0;
	kernel.at<float>(2,2)= 1.0;
	}else{

	// assigns kernel values
	kernel.at<float>(0,0)= -1.0;
	kernel.at<float>(0,1)= -2.0;
	kernel.at<float>(0,2)= -1.0;
	kernel.at<float>(2,0)= 1.0;
	kernel.at<float>(2,1)= 2.0;
	kernel.at<float>(2,2)= 1.0;

	}
	//filter the image
	cv::filter2D(input,grad,CV_16S,kernel);

}


int main(){
//parameter for dmax = c*dmin
int c = 3;
int t_par = 3;

string filename = "Image001";

freopen("/home/teja/Desktop/output","w",stdout);

list<Feature> points;
int unique_num = 0;

 Mat src,src_gray;
 	int height,width;
 	string pth = "/home/teja/Desktop/testimages/";

 	cout << filename << endl;
 	string ext = ".jpg";
 	string tot = pth+filename+ext;
 	src = imread(tot);
 	cvtColor(src,src_gray,CV_RGB2GRAY);
 	height = src.rows;
 	width = src.cols;

 	Mat grad_x,grad_y;

 	sobel_us(src_gray,grad_x,0);
 	sobel_us(src_gray,grad_y,1);

 	Mat grad(grad_x.rows,grad_x.cols,CV_8U);

 	for (int i = 0; i < grad_x.rows; ++i) {
 		for (int j = 0; j < grad_x.cols; ++j) {
 			if(grad_x.at<int16_t>(i,j) != 0 || grad_y.at<int16_t>(i,j) != 0){
 				grad.at<uchar>(i,j) = (abs(grad_x.at<int16_t>(i,j))+abs(grad_y.at<int16_t>(i,j)))/2;
 			}else{
 				grad.at<uchar>(i,j) = 0;
 			}

 		}

 	}

 	//finding threshold
 	  int u = 0;
 	  int n_points = 0;

 	  vector<int> values;

 	  for (int i = 0; i < height; ++i) {
 		  for (int j = 0; j < width; ++j) {
 			  if(grad.at<uchar>(i,j) > 0){
 				  n_points++;
 				  values.push_back(grad.at<uchar>(i,j));
 				  u += grad.at<uchar>(i,j);
 			  }
 		  }
 	  }

 	 u = u/n_points;
 	 int sig = 0;
 	  for(int i=0;i<values.size();i++){
 		  sig += (values[i] - u)*(values[i] - u);
 	  }

 	  sig = sig/n_points;
 	  sig = sqrt(sig);

 	  //threshold if necessary
 	  int t = u + (255 - u)/t_par;

 	  //adaptive one
 	  //t = u+ 1.5*sig;

 	  for (int i = 0; i < height; ++i) {
 		  for (int j = 0; j < width; ++j) {
 			  if(grad.at<uchar>(i,j) < t){
 				  grad.at<uchar>(i,j) = 0;
 			  }
 		}
 	  }


 	 double ornt,wt;
 	 for (int i = 0; i < height; ++i) {
 	  	  for (int j = 0; j < width; ++j) {
 	  		if(grad.at<uchar>(i,j) > 0){
 	  			double temp = 0;
 	  			ornt = atan2(grad_x.at<int16_t>(i,j)+temp,grad_y.at<int16_t>(i,j));

 	  			if(ornt < 0)
 	  				  ornt *= -1;
 	  			else
 	  				  ornt = PI - ornt;

 		  		wt = grad.at<uchar>(i,j);

 	  			Feature f(j,i,wt,ornt,unique_num);
 	  			unique_num++;
 	  			points.push_back(f);

 		  	}


 	  	  }
 	  }

cout << width << " " << height << endl;

string grad_pth = filename+"_grad"+ext;
imwrite(grad_pth,grad);

string resb = intToString(points.size());

doClustering(points,unique_num,c);

//POINTS after clustering
cout << points.size() << endl;

  Mat clust_grad(grad.rows,grad.cols,CV_8UC1);
  for (int i = 0; i < clust_grad.rows; ++i) {
  	  for (int j = 0; j < clust_grad.cols; ++j) {
  		  clust_grad.at<uchar>(i,j) = 0;
  	  }
    }

  list<Feature>::iterator it;
  for(it=points.begin();it!=points.end();it++){
	  Feature f = *it;
	  f.print();
	  clust_grad.at<uchar>((int)f.posy,(int)f.posx) = (uchar)f.grad;
  }

  string resc = intToString(c);

  string resa = intToString(points.size());
  string clust_path = filename+"_clustpc_"+resc+"pb_"+resb+"pa_"+resa+ext;

  imwrite(clust_path,clust_grad);

return 0;

}
