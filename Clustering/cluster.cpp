#include "cluster.h"

int unique_num = 0;

Feature::Feature(double posx,double posy,double grad,double orient,int uni_num){
	this->posx = posx;
	this->posy = posy;
	this->grad = grad;
	this->orient = orient;
	this->uni_num = uni_num;
}

void Feature::print(){
	cout  << (int)posx <<" "<< (int)posy<<" "<< grad<<" "<< orient<<endl;
}

FeaturePair::FeaturePair(Feature f1,Feature f2,double dist){
	this->f1 = f1;
	this->f2 = f2;
	this->dist = dist;
}

double findFeatureDist(Feature f1,Feature f2){
	return sqrt((f1.posx-f2.posx)*(f1.posx-f2.posx) + (f1.posy-f2.posy)*(f1.posy-f2.posy));
}


Feature cluster(Feature f1,Feature f2){
Feature ret;
	double alpha1,alpha2;
	if(abs(f1.orient-f2.orient) <= PI/2){
		alpha1 = f1.orient;
		alpha2 = f2.orient;
	}else if(f1.orient < f2.orient){
		alpha1 = f1.orient + PI;
		alpha2 = f2.orient;
	}else{
		alpha1 = f1.orient;
		alpha2 = f2.orient + PI;
	}


	ret.grad = f1.grad + f2.grad;
	ret.posx = (f1.posx*f1.grad + f2.posx*f2.grad)/ret.grad;
	ret.posy = (f1.posy*f1.grad + f2.posy*f2.grad)/ret.grad;
	ret.orient = (alpha1*f1.grad + alpha2*f2.grad)/ret.grad;

	if(ret.orient > PI)
		ret.orient = ret.orient-PI;

	ret.uni_num = unique_num;
	unique_num++;
return ret;
}


bool iterate(list<FeaturePair> &fps,list<Feature> &points,int d_max){
bool saturated = false;
double min = 100000;

//find minimum in fps
list<FeaturePair>::iterator it1,it2;
list<Feature>::iterator p_it;

FeaturePair min_pair;
for(it1 = fps.begin();it1 != fps.end();it1++){
	FeaturePair fp = *it1;
	if(fp.dist < min){
		min = fp.dist;
		min_pair = fp;
	}

}

if(min > d_max){
	saturated = true;
	return saturated;
}

//remove points from pairs and fps.
Feature f1 = min_pair.f1;
Feature f2 = min_pair.f2;
list<Feature>::iterator rm_pt1,rm_pt2;

for(p_it = points.begin();p_it != points.end();p_it++){
	Feature f = *p_it;
	if(f.uni_num == f1.uni_num){
		rm_pt1 = p_it;
	}
	if(f.uni_num == f2.uni_num){
		rm_pt2 = p_it;
	}
}

points.erase(rm_pt1);
points.erase(rm_pt2);

vector<list<FeaturePair>::iterator> rm_fps;

for(it1 = fps.begin();it1 != fps.end();it1++){
FeaturePair fp = *it1;
if(fp.f1.uni_num == f1.uni_num || fp.f1.uni_num == f2.uni_num ||
		fp.f2.uni_num == f1.uni_num || fp.f2.uni_num == f2.uni_num){
	rm_fps.push_back(it1);
}
}

for(int i=0;i<rm_fps.size();i++){
	fps.erase(rm_fps[i]);
}

//clustered point
Feature newPoint = cluster(min_pair.f1,min_pair.f2);

//find possible feature pairs and add to fps
for(p_it = points.begin();p_it != points.end();p_it++){

	Feature f2 = *p_it;
	double dist = findFeatureDist(newPoint,f2);
	if(dist < d_max){
		FeaturePair fp(newPoint,f2,dist);
		fps.push_back(fp);
	}
}

//create new point and append to pairs
points.push_back(newPoint);

return saturated;
}

list<Feature> doClustering(list<Feature> &points,int u_num,int c){
	unique_num = u_num;
	list<FeaturePair> fps;

	//find d_min
	list<Feature>::iterator it1,it2;
	it1 = points.begin();
	it2 = points.begin();
	int temp = 100000;
	for (it1=points.begin(); it1!=points.end(); ++it1){
		for(it2 = it1;it2!=points.end();++it2){
			if(it2 != it1){
				Feature f1 = *it1;
				Feature f2 = *it2;
				if(findFeatureDist(f1,f2) < temp){
					temp = findFeatureDist(f1,f2);
				}
			}
		}

	}


	int d_min = temp;
	int d_max = c*d_min;

	//iterate over all points and push possible pairs to fps
	it1 = points.begin();
	it2 = points.begin();
	for (it1=points.begin(); it1!=points.end(); ++it1){
		for(it2 = it1;it2!=points.end();++it2){
			if(it1 != it2){
				Feature f1 = *it1;
				Feature f2 = *it2;
				double dist = findFeatureDist(f1,f2);
				if(dist < d_max){
					FeaturePair fp(*it1,*it2,dist);
					fps.push_back(fp);
				}
			}
		}
	}

	//clustering
	bool done = false;
	while(!done){
		done = iterate(fps,points,d_max);
	}

return points;
}









