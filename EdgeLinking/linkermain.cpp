#include "linker.h"

string intToString(int x){
	 ostringstream convert;
	  convert << x;
	  string res = convert.str();
	  return res;
}

string doubleTostring(double angle){
	double deg = angle*180/PI;
	int degree = (int)deg;
	return intToString(degree);
}



int main(){
//constant for dmax = c*dmin
int c = 3;
double a_max = 0.61;

string filename;
string ext = ".jpg";

freopen("/home/teja/Desktop/output","r",stdin);

list<Feature> points;
int cols,rows;
int num_points;
int unique_num = 0;

//read filename
cin >> filename;

//read points from file
cin >> cols >> rows;
cin >> num_points;

list<Feature>::iterator it;

for(int i=0;i<num_points;i++){
	double wt,posx,posy,orient;
	cin >> posx >> posy >> wt >> orient ;

	Feature f(posx,posy,wt,orient,unique_num);
	unique_num++;
	points.push_back(f);
}

//do linking
list<Path> paths = doLinking(points,c,a_max);

//show output after linking process
Mat link_grad(rows,cols,CV_8UC1);
  for (int i = 0; i < link_grad.rows; ++i) {
  	  for (int j = 0; j < link_grad.cols; ++j) {
  		  link_grad.at<uchar>(i,j) = 0;
  	  }
    }

  for(it=points.begin();it!=points.end();it++){
	  Feature f = *it;
	  link_grad.at<uchar>((int)f.posy,(int)f.posx) = (uchar)f.grad;
  }

  list<Path>::iterator itp;
  list<Feature>::iterator itf;
  	for(itp=paths.begin();itp!=paths.end();itp++){
  		Path path = *itp;
  		path.draw(link_grad);
  	}

 string res1 = intToString(c);
 string res2 = doubleTostring(a_max);
 string clust_path = filename+"_linkpc_"+res1+"linkpa_"+res2+ext;

 imwrite(clust_path,link_grad);
 waitKey(-1);

}





