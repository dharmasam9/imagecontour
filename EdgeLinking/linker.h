#ifndef LINKER_H_
#define LINKER_H_

#define PI 3.14

#include "stdlib.h"
#include "math.h"
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;


//data structures
class Feature{
public:
	double posx;
	double posy;
	double grad;
	double orient;
	bool link;
	int uni_num;
	Feature(){};
	Feature(double,double,double,double,int);
	void print();

};

class Vect{
public:
	double x,y;
	Vect(double,double);
};


class FeaturePair{
public:
	Feature f1;
	Feature f2;
	double weight;
	FeaturePair(){};
	FeaturePair(Feature,Feature,double);
};

class Path{
public:
	list<Feature> points;
	Path(){};
	void addback(Feature);
	void addfront(Feature);
	void draw(Mat &);
	void print();

};

list<Path> doLinking(list<Feature> &points,int,double);


#endif /* LINKER_H_ */
