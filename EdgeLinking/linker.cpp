
#include "linker.h"
//--PATH functions
void Path::addback(Feature f){
		points.push_back(f);
	}
void Path::addfront(Feature f){
		points.push_front(f);
	}
void Path::draw(Mat &link_grad){
	list<Feature>::iterator it1,it2;
	Feature f1 = points.front();
	for(it1=points.begin();it1!=points.end();it1++){
		if(it1 != points.begin()){
			Feature f2 = *it1;
			if(f1.posx > 1 && f1.posy > 1 &&f2.posx > 1 &&f2.posy > 1 ){

				line(link_grad,Point((int)f1.posx,(int)f1.posy),Point((int)f2.posx,(int)f2.posy),Scalar(255,255,255));
			}else{
				if(f1.posx > 1 && f1.posy > 1)
					link_grad.at<uchar>((int)f1.posy,(int)f1.posx) = 0;
				if(f2.posx > 1 && f2.posy > 1)
					link_grad.at<uchar>((int)f2.posy,(int)f2.posx) = 0;
			}
			f1 = f2;
		}
	}
}

void Path::print(){
	list<Feature>::iterator it1;
	for(it1=points.begin();it1!=points.end();it1++){
		Feature f = *it1;
		cout <<"("<< f.posx <<","<< f.posy << ")";
	}
	cout << endl;
}

//VECTOR functions
Vect::Vect(double x,double y){
this->x = x;
this->y = y;
}

//FEATURE functions
Feature::Feature(double posx,double posy,double grad,double orient,int uni_num){
	this->posx = posx;
	this->posy = posy;
	this->grad = grad;
	this->orient = orient;
	this->uni_num = uni_num;
	this->link = false;
}

void Feature::print(){
	cout  << posx <<" "<< posy<<" "<< grad<<" "<< orient<<endl;
}

//FEATURE PAIR functions
FeaturePair::FeaturePair(Feature f1,Feature f2,double weight){
	this->f1 = f1;
	this->f2 = f2;
	this->weight = weight;
}

//HELPER functions

//distance between two oriented edge points
double findFeatureDist(Feature f1,Feature f2){
	return sqrt((f1.posx-f2.posx)*(f1.posx-f2.posx) + (f1.posy-f2.posy)*(f1.posy-f2.posy));
}

//Angle{0 - PI} made by line joining two edge points
double findFeatureOrient(Feature f1,Feature f2){
	double den = f1.posx - f2.posx;
	double num = f1.posy - f2.posy;
	double orient = atan2(num,den);

	if(orient < 0)
		orient += PI;

	return orient;

}

//To find angle between two vectors
double angleOfVectors(Vect v1,Vect v2){
	double num = (v1.x*v2.x + v1.y*v2.y);
	double den = sqrt(v1.x*v1.x + v1.y*v1.y)*sqrt(v2.x*v2.x + v2.y*v2.y);
	return acos(num/den);
}

//For Discriminating linking points
void setFlagPoint(Feature f,list<Feature> &points){
	int unique_num = f.uni_num;
	bool done = false;
	list<Feature>::iterator it1;
	for(it1=points.begin();it1!=points.end() && !done;it1++){
		if((*it1).uni_num == unique_num){
			(*it1).link = true;
			done = true;
		}
	}

}


Feature findSegmentPoint(Feature f1,list<Feature> &points,double d_max,double a_max){
list<Feature>::iterator it1;
list<FeaturePair> eligible;

for(it1=points.begin();it1!=points.end();it1++){
	Feature f2 = *it1;
	if(!f2.link){
		double dist = findFeatureDist(f1,f2);
		double orient = findFeatureOrient(f1,f2);
		if(dist <= d_max && abs(f1.orient - orient) < a_max &&
			abs(f2.orient - orient) < a_max){
		double w_dist = 1 - dist/d_max;
		double w_alpha1 = 1 -  abs(f1.orient - orient)/a_max;
		double w_alpha2 = 1 - abs(f2.orient - orient)/a_max;
		double weight,temp;
		if(w_dist > w_alpha1)
			temp = w_alpha1;
		else
			temp = w_dist;
		if(temp > w_alpha2)
			weight = w_alpha2;
		else
			weight = temp;

		FeaturePair fp(f1,f2,weight);
		eligible.push_back(fp);
		}
	}
}

list<FeaturePair>::iterator it2;
FeaturePair fp;
double max = 0;
for(it2=eligible.begin();it2!=eligible.end();it2++){
	FeaturePair temp = *it2;
	if(temp.weight > max){
		fp = temp;
		max = fp.weight;
	}
}

if(eligible.size() == 0)
	return f1;
else
	return fp.f2;

}


Feature findNextPoint(Feature f1,Feature f2,list<Feature> &points,double d_max,double a_max){
list<Feature>::iterator it1;
list<FeaturePair> eligible;

//calculate vector of f1 and f2
Vect v1(f2.posx-f1.posx,f2.posy-f1.posy);

for(it1=points.begin();it1!=points.end();it1++){
	Feature f3 = *it1;
	if(!f3.link){
		double dist = findFeatureDist(f2,f3);
		double orient = findFeatureOrient(f2,f3);
		Vect v2(f3.posx-f2.posx,f3.posy-f2.posy);

		if(!(dist > d_max || abs(f1.orient - orient) > a_max
			||abs(f2.orient - orient) > a_max || angleOfVectors(v1,v2) > PI/2)){

		double w_dist = 1 - dist/d_max;
		double w_alpha1 = 1 -  abs(f1.orient - orient)/a_max;
		double w_alpha2 = 1 - abs(f3.orient - orient)/a_max;
		double weight;
		if(w_dist > w_alpha1)
			weight = w_alpha1;
		else
			weight = w_dist;
		if(weight > w_alpha2)
			weight = w_alpha2;

		FeaturePair fp(f2,f3,weight);
		eligible.push_back(fp);
		}
	}
}

list<FeaturePair>::iterator it2;
FeaturePair fp;
double max = 0;
for(it2=eligible.begin();it2!=eligible.end();it2++){
	FeaturePair temp = *it2;
	if(temp.weight > max){
		fp = temp;
		max = fp.weight;
	}
}
if(eligible.size() == 0)
	return f1;
else
	return fp.f2;

}

list<Path> doLinking(list<Feature> &points,int c,double a_max){

	double d_max;

	//setting d_max
	list<Feature>::iterator it1,it2;
	double d_min = 100000000;
	for(it1=points.begin();it1!=points.end();it1++){
			Feature f1 = *it1;
			for(it2=it1;it2!=points.end();it2++){
				if(it2 != it1){
					Feature f2 = *it2;
					double dist = findFeatureDist(f1,f2);
					if(dist < d_min){
						d_min = dist;
					}
				}

			}
	}

	d_max = c*d_min;

	cout << d_min << endl;

	list<Path > paths;

	for(it1=points.begin();it1!=points.end();it1++){
		Feature f1 = *it1;
		if(!f1.link){
			Path path;
			setFlagPoint(f1,points);
			path.addback(f1);

			Feature f2 =findSegmentPoint(f1,points,d_max,a_max);
			if(f2.uni_num != f1.uni_num){

			setFlagPoint(f2,points);
			path.addback(f2);

			//right side f2
			bool rightend = false;
			Feature rf1 = f1,rf2 = f2;
			while(!rightend){
				Feature temp = findNextPoint(rf1,rf2,points,d_max,a_max);
				if(temp.uni_num == rf1.uni_num)
					rightend = true;
				else{
					setFlagPoint(temp,points);
					path.addback(temp);
					rf1 = rf2;
					rf2 = temp;
				}
			}

			//left side f1
			Feature lf1 = f2,lf2 = f1;
			bool leftend = false;
			while(!leftend){
				Feature temp = findNextPoint(lf1,lf2,points,d_max,a_max);
				if(temp.uni_num == lf1.uni_num)
					leftend = true;
				else{
					setFlagPoint(temp,points);
					path.addfront(temp);
					lf1 = lf2;
					lf2 = temp;
				}
			}
			}
			paths.push_back(path);
			path.print();

		}


	}

	cout << paths.size() << endl;

	list<Path>::iterator itp;
	for(itp=paths.begin();itp!=paths.end();itp++){
		Path p = *itp;
		//p.print();
	}

	return paths;

}


